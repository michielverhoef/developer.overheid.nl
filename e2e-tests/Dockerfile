FROM node:10.16.0-slim

ENV CI=true

# prerequisites for Puppeteer
# via https://github.com/GoogleChrome/puppeteer/blob/de82b87cfa1c637787a09b90266318905ae16f42/docs/troubleshooting.md#running-puppeteer-in-docker

# See https://crbug.com/795759
RUN apt-get update && apt-get install -yq curl libgconf-2-4

# Install latest chrome dev package and fonts to support major charsets (Chinese, Japanese, Arabic, Hebrew, Thai and a few others)
# Note: this installs the necessary libs to make the bundled version of Chromium that Puppeteer
# installs, work.
RUN apt-get update && apt-get install -y wget --no-install-recommends \
    && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
    && apt-get update \
    && apt-get install -y google-chrome-unstable fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst ttf-freefont \
      --no-install-recommends \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /src/*.deb

COPY e2e-tests/wait-for-http.sh \
     /go/src/commonground/developer.overheid.nl/e2e-tests/

# Uncomment to skip the chromium download when installing puppeteer. If you do,
# you'll need to launch puppeteer with:
#     browser.launch({executablePath: 'google-chrome-unstable'})
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true

# Copy only package.json and package-lock.json to make the dependency fetching step optional.
COPY e2e-tests/package.json \
     e2e-tests/package-lock.json \
     /go/src/commonground/developer.overheid.nl/e2e-tests/

WORKDIR /go/src/commonground/developer.overheid.nl/e2e-tests

RUN npm ci --no-progress --color=false --quiet

# Now copy the whole workdir.
COPY e2e-tests /go/src/commonground/developer.overheid.nl/e2e-tests
